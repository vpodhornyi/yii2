<?php

namespace common\components;

use Yii;


class StringHelper
{
    private $limit;

    function __construct()
    {
        $this->limit = Yii::$app->params['shortTextLimit'];
    }

    public function getShort($string, $limit = null)
    {
        if ($limit === null) {
            $limit = $this->limit;
        }
        $string = substr($string, 0, $limit);
        $string .= '...';
        return $string;
    }
}