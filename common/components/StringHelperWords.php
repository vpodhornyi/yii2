<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 04.06.2017
 * Time: 11:04
 */

namespace common\components;

use Yii;

class StringHelperWords
{
    private $limitSymbols;
    private $limitWords;

    function __construct()
    {
        $this->limitSymbols = Yii::$app->params['symbolsLimit'];
        $this->limitWords = Yii::$app->params['wordsLimit'];
    }

    public function getShortBySymbols($string, $limitSymbols = NULL)
    {
        if ($limitSymbols === null) {
            $limitSymbols = $this->limitSymbols;
        }
        $limitSymbols = $string[$limitSymbols] === ' '? $limitSymbols: stripos($string, ' ', $limitSymbols);
        $string = substr($string, 0, $limitSymbols);
        $string .= '...';
        return $string;
    }

    public function getShortByWords($string, $limitWords = NULL)
    {
        if ($limitWords === null) {
            $limitWords = $this->limitWords;
        }
        $count = $limit = 0;
        while ($count < $limitWords) {
            $limit = strpos($string, ' ', ++$limit);
            $count++;
        }
        $string = substr($string, 0, $limit);
        $string .= '...';
        return $string;
    }
}