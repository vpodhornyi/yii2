<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 17.06.2017
 * Time: 20:50
 */

namespace common\models;

use Yii;
use yii\base\Model;

class Employee extends Model
{
    public $name;
    public $surname;
    public $email;
    public $salary;
    
    public static function getList()
    {
        $sql = "SELECT name, surname, email, salary FROM employee";
        return Yii::$app->db->createCommand($sql)->queryAll();
    }

}