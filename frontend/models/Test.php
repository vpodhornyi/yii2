<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 21.05.2017
 * Time: 21:56
 */

namespace frontend\models;

use Yii;


class Test
{
    /**
     * @param integer $max
     * @return array
     */
    public static function getNewsList($max)
    {
        $max = intval($max);
        $sql = 'SELECT * FROM news LIMIT ' . $max;

        $result = Yii::$app->db->createCommand($sql)->queryAll();

        if (!empty($result) && is_array($result)) {
            foreach ($result as &$value) {
                $value['content'] = Yii::$app->stringHelperWords->getShortByWords($value['content']);
            }
        }
        return $result;
    }

    /**
     * @param integer $id
     * @return array|false
     */
    public static function getItem($id)
    {
        $sql = 'SELECT * FROM news WHERE id = ' . $id;

        $result = Yii::$app->db->createCommand($sql)->queryOne();

        return $result;
    }

    public static function getCountNews()
    {
        $sql = 'SELECT count(id) FROM news';

        $result = Yii::$app->db->createCommand($sql)->queryScalar();

        return $result;
    }
}