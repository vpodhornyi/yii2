<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 05.06.2017
 * Time: 20:44
 */

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Test;

class CountController extends Controller
{
    public function actionCountNews()
    {
        $count =  Test::getCountNews();

        return $this->render('index', ['count' => $count]);
    }
}