<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 21.05.2017
 * Time: 21:10
 */

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Test;


class TestController extends Controller
{
    public function actionIndex()

    {

        $max = Yii::$app->params['maxNewsInList'];

        $list = Test::getNewsList($max);

        return $this->render('index', [
            'list' => $list,
        ]);
    }

    public function actionMail()
    {
        $result = Yii::$app->mailer->compose()
            ->setFrom('2408ua@gmail.com')
            ->setTo('wxs@ukr.net')
            ->setSubject('Yii mailer')
            ->setTextBody('Test')
//            ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
            ->send();
        var_dump($result);
    }

    public function actionView($id)
    {
        $item = Test::getItem($id);

        return $this->render('view', [
            'item' => $item
        ]);
    }
}