<?php

namespace frontend\controllers;

use yii\web\Controller;
use common\models\Employee;

class EmployeeController extends Controller
{

    public function actionIndex()
    {
        $employee = new Employee();
        $employee->name = 'Bob';
        $employee->surname = 'Marley';
        $employee->email = 'wxs@ukr.net';
        $employee->salary = 15000;
        echo '<pre>';
        print_r($employee);
        print_r($employee->toArray());

    }
}