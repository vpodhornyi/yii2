<?php
return [
    'adminEmail' => 'admin@example.com',
    'maxNewsInList' => 2,
    'shortTextLimit' => 20,
    'symbolsLimit' => 10,
    'wordsLimit' => 5,
];
