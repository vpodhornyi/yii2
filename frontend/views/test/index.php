<?php
use yii\helpers\Url;

foreach ($list as $item): ?>

<h1>
    <a href="<?= Yii::$app->urlManager->createUrl(['test/view', 'id' => $item['id']])?>"><?= $item['titel'];?></a>
</h1>
<p><?= $item['content'];?></p>

<?php endforeach;?>
<a href="<?= Url::to(['/'])?>"  class="btn btn-info">Back</a>

