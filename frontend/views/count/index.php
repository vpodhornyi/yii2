<?php
use yii\helpers\Url;
?>
<div class="jumbotron">
    <p class="lead">The table "news" has <?= $count?> news! </p>
    <a href="<?= Url::to(['/'])?>"  class="btn btn-info">Back</a>
</div>
