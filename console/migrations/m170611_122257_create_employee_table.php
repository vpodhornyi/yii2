<?php

use yii\db\Migration;

/**
 * Handles the creation of table `employee`.
 */
class m170611_122257_create_employee_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('employee', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'surname' => $this->string(50),
            'date_of_birthday' => $this->date(),
            'city' => $this->string(50),
            'start_of_work_date' => $this->date(),
            'length_of_service' => $this->integer(),
            'appointment' => $this->string(100),
            'department_number' => $this->integer(),
            'ID_number' => $this->string(20),
            'email' => $this->string(100)->unique(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('employee');
    }
}
