<?php

namespace console\controllers;

use Yii;

class TimeController extends \yii\console\Controller
{
    public function actionGetTime()
    {
        $fp = fopen("/var/www/project/frontend/web/log.txt", "a");
        $date = Yii::$app->formatter->asDatetime('now', 'long');
        fwrite($fp, $date . "\n");
        fclose($fp);
    }
}