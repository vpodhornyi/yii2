<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 17.06.2017
 * Time: 13:20
 */

namespace console\controllers;

use Yii;
use yii\helpers\Console;
use common\models\Employee;
use console\models\News;
use console\models\Subscriber;
use console\models\Sender;


class MailerController extends \yii\console\Controller
{
    public function actionSend()
    {
        $newsList = News::getList();
        $subscribers = Subscriber::getList();

        $count = Sender::run($subscribers, $newsList);
        Console::output("\nEmails sent: {$count}");
    }

    public  function actionSendSalaryNotification()
    {
        $employees = Employee::getList();

        $count = Sender::salaryNotification($employees);
        Console::output("\nEmails sent: {$count}");
    }
}