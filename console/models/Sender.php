<?php


namespace console\models;

use Yii;


class Sender
{
    public static function run($subscribers, $newsList = NULL)
    {
        $count = 0;
        foreach ($subscribers as $subscriber) {
            $result = Yii::$app->mailer->compose('/mailer/newslist', [
                'newsList' => $newsList,
            ])
                ->setFrom('2408ua@gmail.com')
                ->setTo($subscriber['email'])
                ->setSubject('Yii mailer')
                ->send();
            if ($result)
                $count++;
        }
        return $count;
    }

    public static function salaryNotification($employees)
    {
        $count = 0;
        foreach ($employees as $employee) {
            $result = Yii::$app->mailer->compose('/mailer/salaryNotification', [
                'employee' => $employee,
            ])
                ->setFrom('2408ua@gmail.com')
                ->setTo($employee['email'])
                ->setSubject('Yii mailer')
                ->send();
            if ($result)
                $count++;
            Logs::actionSalaryNotification($employee);
        }
        return $count;
    }
}