<?php

namespace console\models;

use Yii;

class Logs
{

    public static function actionSalaryNotification($employee)
    {
        $fp = fopen("/var/www/project/console/logs/log.txt", "a");
        $date = Yii::$app->formatter->asDatetime('now', 'long') . ', Salary: ' . $employee['salary'] . '$, Send to: ' .
            $employee['name'] . ' ' . $employee['surname'];
        fwrite($fp, $date . "\n");
        fclose($fp);
    }
}