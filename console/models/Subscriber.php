<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 17.06.2017
 * Time: 17:33
 */

namespace console\models;

use Yii;

class Subscriber
{
    public static function getList()
    {
        $sql = 'SELECT * FROM subscriber';
        return Yii::$app->db->createCommand($sql)->queryAll();
    }

}