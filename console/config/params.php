<?php
return [
    'adminEmail' => 'admin@example.com',
    'shortTextLimit' => 20,
    'symbolsLimit' => 10,
    'wordsLimit' => 10,
];
